const gql = require('graphql');
const Subarticle = require('./subarticle')

const Article = new gql.GraphQLObjectType({
    name: "Article",
    fields: () => ({
        id: {
            type: gql.GraphQLID
        },
        title: {
            type: gql.GraphQLString
        },
        link: {
            type: gql.GraphQLString
        },
        created_at: {
            type: gql.GraphQLFloat,
            resolve(obj){
                return obj.created_at.getTime()
            }
        },
        updated_at: {
            type: gql.GraphQLFloat,
            resolve(obj){
                return obj.updated_at.getTime()
            }
        },
        next: {
            type: Article,
            resolve(root) {
                return root.getNext()
            } 
        },
        previous: {
            type: Article,
            resolve(root) {
                return root.getPrevious()
            }
        },
        subarticles: {
            type: gql.GraphQLList(Subarticle),
            resolve(root) {
                return root.getSubarticles()
            }
        }
    })
});

module.exports = Article


// const Message = new GraphQLObjectType({
//     name: 'Message',
//     fields: () => ({
//         comments: {
//             type: new GraphQLList(Message),
//             resolve(message) {
//                 return getComments(message.id);
//             },
//         },
//         content: {
//             type: GraphQLString,
//         },
//     }),
// });