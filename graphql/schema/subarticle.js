const gql = require('graphql');
const Paragraph = require('./paragraph')

const Subarticle = new gql.GraphQLObjectType({
    name: "Subarticle",
    fields: () => ({
        id: {
            type: gql.GraphQLID
        },
        title: {
            type: gql.GraphQLString
        },
        paragraphs: {
            type: gql.GraphQLList(Paragraph),
            resolve(root) {
                return root.getParagraphs()
            }
        }
    })
});

module.exports = Subarticle