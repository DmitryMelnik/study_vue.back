const gql = require('graphql');


const ParagraphText = new gql.GraphQLObjectType({
    name: "ParagraphText",
    fields: {
        content: {
            type: gql.GraphQLString
        },
        type: {
            type: new gql.GraphQLEnumType({
                name: 'text_type',
                values: {
                    text: { value: 'text' },
                    info: { value: 'info' },
                    image: { value: 'image' }
                }
            })
        }
    }
});


const ParagraphCode = new gql.GraphQLObjectType({
    name: "ParagraphCode",
    fields: {
        content: {
            type: gql.GraphQLString
        },
        language: {
            type: new gql.GraphQLEnumType({
                name: 'language',
                values: {
                    html: { value: 'html' },
                    js: { value: 'js' }
                }
            })
        },
        filename: {
            type: gql.GraphQLString
        },
        isCode: {
            type: gql.GraphQLBoolean,
            resolve() {
                return true
            }
        }
    }
});

const ParagraphEditor = new gql.GraphQLObjectType({
    name: "ParagraphEditor",
    fields: {
        html: {
            type: gql.GraphQLString
        },
        filenameHTML: {
            type: gql.GraphQLString
        },
        javascript: {
            type: gql.GraphQLString
        },
        filenameJS: {
            type: gql.GraphQLString
        },
        isEditor: {
            type: gql.GraphQLBoolean,
            resolve() {
                return true
            }
        }
    }
});


const Paragraph = new gql.GraphQLUnionType({
    name: 'Paragraph',
    types: [ParagraphText, ParagraphCode, ParagraphEditor],
    resolveType(value) {
        if (!!value.content && !!value.type) {
            return ParagraphText
        }
        else if (!!value.content && !!value.language) {
            return ParagraphCode
        }
        else if (!!value.html && !!value.javascript) {
            return ParagraphEditor
        }
    }
})


module.exports = Paragraph