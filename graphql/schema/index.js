const gql = require('graphql')
const db = require('../../db')

const article = require('./article')

var queryType = new gql.GraphQLObjectType({
    name: 'Query',
    fields: {
        articles: {
            type: new gql.GraphQLList(article),
            args: {
                id: {
                    type: gql.GraphQLID
                }
            },
            resolve(root, args){
                return db.models.articles.findAll({where: args})
            }
        }
    }
})


var schema = new gql.GraphQLSchema({
    query: queryType
})

module.exports = schema