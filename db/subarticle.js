const Sequelize = require('sequelize')

const subarticle = {
    title: {
        type: Sequelize.STRING,
        allowNull: false
    }
}

module.exports = subarticle