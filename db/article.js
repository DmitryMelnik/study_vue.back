const Sequelize = require('sequelize')

const article = {
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    link: {
        type: Sequelize.STRING,
        allowNull: false
    }
}

module.exports = article