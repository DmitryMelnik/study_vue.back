const Sequelize = require('sequelize')
const Faker = require('faker')

const article  = require('./article')
const subarticle  = require('./subarticle')
const paragraph = require('./paragraph/paragraph')
// const paragraphText = require('./paragraph/paragraphText')
// const paragraphCode = require('./paragraph/paragraphCode')
// const paragraphEditor = require('./paragraph/paragraphEditor')

const Conn = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASS,
    {
        dialect: 'mysql',
        host: process.env.DB_HOST,
        operatorsAliases: Sequelize.Op
    }
)

const Article = Conn.define('articles', article, {underscored: true})
Article.belongsTo(Article, {as: 'next'})
Article.belongsTo(Article, {as: 'previous', })

const Subarticle = Conn.define('subarticles', subarticle, {underscored: true})
Article.hasMany(Subarticle, {as: 'subarticles'})

const Paragraph = Conn.define('paragraphs', paragraph, {underscored: true})
// const ParagraphText = Conn.define('paragraphs_texts', paragraphText, {underscored: true})
// const ParagraphCode = Conn.define('paragraphs_codes', paragraphCode, {underscored: true})
// const ParagraphEditor = Conn.define('paragraphs_editors', paragraphEditor, {underscored: true})

// ParagraphText.belongsTo(Paragraph, {as: 'text', foreignKey: 'id'})
// ParagraphCode.belongsTo(Paragraph, {as: 'code', foreignKey: 'id'})
// ParagraphEditor.belongsTo(Paragraph, {as: 'editor', foreignKey: 'id'})

Subarticle.hasMany(Paragraph, {as: 'paragraphs'})

// Subarticle.hasMany(ParagraphText, {as: 'paragraphsTexts'})
// Subarticle.hasMany(ParagraphCode, {as: 'paragraphsCodes'})
// Subarticle.hasMany(ParagraphEditor, {as: 'paragraphsEditors'})

// Conn.sync({ force: true }).then(() => {
//     for (let i = 0; i < 10; i++) {
//         Article.create({
//             title: Faker.name.title(),
//             link: '/'+Faker.internet.domainWord(),
//             next_id: Faker.random.number({min:1, max:i+1}),
//             previous_id: Faker.random.number({min:1, max:i+1})
//         })
//     }
// })

module.exports = Conn