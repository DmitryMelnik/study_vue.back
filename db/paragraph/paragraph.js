const Sequelize = require('sequelize')

const paragraph = {
    content: {
        type: Sequelize.TEXT
    },
    type: {
        type: Sequelize.ENUM('text', 'info', 'image')
    },
    language: {
        type: Sequelize.ENUM('html', 'js')
    },
    filename: {
        type: Sequelize.STRING
    },
    html: {
        type: Sequelize.TEXT
    },
    javascript: {
        type: Sequelize.TEXT
    },
    filenameJS: {
        type: Sequelize.STRING
    },
    filenameHTML: {
        type: Sequelize.STRING
    }
}

module.exports = paragraph