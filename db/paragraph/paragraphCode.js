const Sequelize = require('sequelize')

const paragraphCode = {
    content: {
        type: Sequelize.TEXT,
        allowNull: false
    },
    language: {
        type: Sequelize.ENUM('html', 'js'),
        allowNull: false
    }
}

module.exports = paragraphCode