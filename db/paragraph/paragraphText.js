const Sequelize = require('sequelize')

const paragraph = {
    content: {
        type: Sequelize.TEXT,
        allowNull: false
    },
    type: {
        type: Sequelize.ENUM('text', 'info', 'image'),
        allowNull: false
    }
}

module.exports = paragraph