const Sequelize = require('sequelize')

const paragraphEditor = {
    html: {
        type: Sequelize.TEXT,
        allowNull: false
    },
    javascript: {
        type: Sequelize.TEXT,
        allowNull: false
    }
}

module.exports = paragraphEditor