require('dotenv').config()
const express = require('express')
const graphQL = require('express-graphql')
const schema = require('./graphql/schema')
const cors = require('cors')

var app = express();
app.use(cors())

app.use('/graphql', graphQL({ schema: schema, pretty: true, graphiql: true }))
app.listen(process.env.APP_POST, function () {
    console.log('Server start http://localhost:' + process.env.APP_POST + '/ !')
})

